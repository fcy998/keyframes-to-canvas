# KeyframesToCanvas

#### 介绍
```
简单的序列帧播放工具
因为近期做官网项目时刚好需要类似的功能,就随手写了一个小工具
（注：本工具不依赖jq等库，单纯的js所写，示例中引入jq只是方便使用ajax进行本地的请求）
```
#### 版本说明
```
v1.7 版本说明
 *新增实例方法setWidth和setHeight
```

```
v1.6 版本说明
 *优化部分代码以及页面展示
```

```
v1.5 版本说明
 *新增配置scX和scY两个缩放属性，scX为水平缩放，scY为垂直播放，同时新增新实例方法，setZoom，可在外部进行舞台缩放
```

```
v1.4 版本说明
 *新增配置属性loop，是否循环播放，默认为false，true为开启循环，支持正序/倒序循环
```

```
v1.3 版本说明
 *新增指定帧数开始播放方法goToAndPlay与指定帧数位置停止方法goToAndStop
 *新增配置属性reverse，默认为false，true为倒序播放，false为正序播放
```

```
v1.2 版本说明
 *修复部分方法不存在默认值导致的错误
 *修复动画停止后再次点击开始导致的错误
 *新增动画重置功能

```

```
v1.1 版本说明
 *新增独立调用方法start、active、stop
 *修改配置对象中三种状态回调的名称
```
#### 软件架构
```
JavaScript
```


#### 安装教程

##### 直接下载dist文件夹下res里面的js文件就能使用

#### 使用说明
- 引入文件后直接实例化工具的对象，传入三个参数，参数说明如下：
  - nodeName - 容器节点ID或者Class类名
  - imageArray - 动画序列帧数组
  - options - 配置信息
  - 配置参数目前支持
  - speed(播放速度)、x(x位置)、y(y位置)、width(宽)、height(高)、scX(水平缩放)、scY(垂直缩放)
  - loop(是否循环播放)、
  - reverse(是否正序/倒序)、
  - startCallback回调(返回当前canvas实例)、
  - middleCallback回调(返回当前播放中动画的帧数)、
  - endCallback回调(返回当前canvas实例)

```
new KeyframesToCanvas(nodeName,imageArray,options)
```

#### 代码示例

```
	<!DOCTYPE html>
	<html>
		<head>
			<meta charset="utf-8" />
			<title></title>
		</head>
		<style type="text/css">
			#keyframes{
				width: 100%;
				height: 100vh;
				margin: 0 auto;
			}
			#stopan,#start,#reset,#goto,#gotostop,#sc_btn,#width_btn,#height_btn{
				width: 100px;
				height: 50px;
				position: fixed;
				left: 50px;
				top: 100px;
				z-index: 2020;
			}
			#start{
				left: 150px;
			}
			#reset{
				left: 250px;
			}
			#fps{
				width: 100px;
			}
			#goto{
				left: 630px;
			}
			#gotostop{
				left: 730px;
			}
			#sc_btn{
				left: 1015px;
			}
			#width_btn{
				height: 30px;
				left: 235px;
				top: 175px;
			}
			#height_btn{
				height: 30px;
				left: 235px;
				top: 215px;
			}
			.show_x_y{
				position: absolute;
				left: 1135px;
				top: 95px;
				margin: 0;
			}
			.show_x_y>p{
				margin: 0;
			}
			.fps_box1,.fps_box2{
				width: 400px;
				position: absolute;
				left: 370px;
				top: 95px;
			}
			.fps_box1>input{
				width: 20%;
			}
			.fps_box2>input{
				width: 20%;
			}
			.fps_box2{
				top: 130px;
			}
			.sc_x,.sc_y{
				width: 400px;
				position: absolute;
				left: 845px;
				top: 95px;
			}
			.sc_width,.sc_height{
				width: 400px;
				position: absolute;
				left: 50px;
				top: 175px;
			}
			.sc_x>input{
				width: 20%;
			}
			.sc_y>input{
				width: 20%;
			}
			.sc_y{
				top: 130px;
			}
			.sc_height{
				top: 220px;
			}
			.sc_width>input{
				width: 20%;
			}
			.sc_height>input{
				width: 20%;
			}
		</style>
		<body>
			<div id="keyframes"></div>
			<button type="button" id="stopan">点击停止动画</button>
			<button type="button" id="start">点击开始动画</button>
			<button type="button" id="reset">点击重置动画</button>
			<div class="fps_box1">
				<span>输入指定播放帧数：</span>
				<input type="text" name="" id="fps1" value="" />
			</div>
			<div class="fps_box2">
				<span>输入指定停止帧数：</span>
				<input type="text" name="" id="fps2" value="" />
			</div>
			<button type="button" id="goto">指定帧数位置开始播放</button>
			<button type="button" id="gotostop">指定帧数位置停止</button>
			<div class="sc_x">
				<span>水平缩放</span>
				<input type="text" name="" id="sc_x" value="" placeholder="dx"/>
			</div>
			<div class="sc_y">
				<span>垂直缩放</span>
				<input type="text" name="" id="sc_y" value="" placeholder="dy"/>
			</div>
			<button type="button" id="sc_btn">缩放</button>
			<div class="sc_width">
				<span>画布宽度</span>
				<input type="text" name="" id="sc_width" value="" placeholder="width"/>
			</div>
			<div class="sc_height">
				<span>画布高度</span>
				<input type="text" name="" id="sc_height" value="" placeholder="height"/>
			</div>
			<button type="button" id="width_btn">修改宽度</button>
			<button type="button" id="height_btn">修改高度</button>
		</body>
	</html>
```

```
let arr = '#keyframes'
let keyframes = document.getElementById('keyframes')
let canvas = null;
$.ajax({
	url:'./src/res.json',
	method:'get',
	success:function(res){
		canvas = new KeyframesToCanvas(arr,res.images,{
			speed:60,
			x:keyframes.clientWidth/2,
			y:keyframes.clientHeight/2,
			width:keyframes.clientWidth,
			height:keyframes.clientHeight,
			reverse:false,
			loop:false,
			scX:1,
			scY:0.5,
			startCallback:function(res){
				console.log(res)
			}
		})
	}
})
```

```
//序列帧数组示例,此为json格式，这里是我进行数据模拟所演示
{
	"code":"200",
	"images":[
		{
			"name":"man-0",
			"src":"src/img/image/171-1.png"
		},
		{
			"name":"man-1",
			"src":"src/img/image/171-2.png"
		}
	]
}
```
#### 同时也支持通过实例调用相应方法
| 方法       | 功能       |
| --------   | -----:    |
| start      | 开始播放   |
| active     | 动画执行中 |
| stop       | 停止动画   | 
| reset      | 重置动画   |
| goToAndPlay| 指定帧数位置播放   |
| goToAndStop| 指定帧数位置停止   |
| setZoom    | 根据给定参数进行画布缩放   |
| setWidth    | 根据给定参数修改画布及容器的宽度   |
| setHeight   | 根据给定参数修改画布及容器的高度   |

###### 使用方式
```
let canvas = new KeyframesToCanvas({
	...
})
```
```
canvas.start()

canvas.start((res)=>{
	console.log('工具实例',res)
})
```

```
canvas.active = (res) => {
	console.log('动画执行中，获取当前播放帧数',res)
}
```

```
canvas.stop()

canvas.stop((res)=>{
	console.log('工具实例',res)
})
```

```
canvas.reset()
```

```
canvas.goToAndPlay(10)
```

```
canvas.goToAndStop(20)
```

```
canvas.setZoom(0.5,0.5)
```

```
canvas.setWidth(600)
```

```
canvas.setHeight(600)
```