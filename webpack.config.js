const path = require('path')
const {
	CleanWebpackPlugin
} = require('clean-webpack-plugin')
let dirTime = new Date();
let dirName = `${dirTime.getMonth()+1}-${dirTime.getDate()}-${dirTime.getHours()}：${dirTime.getMinutes()}：${dirTime.getSeconds()}`
module.exports = {
	entry: './src/js/KeyframesToCanvas.js',
	output: {
		filename: 'KeyframesToCanvas.min.js',
		path: path.resolve(__dirname, `dist`)
	},
	module: {
		rules: [{
			test: /\.js$/,
			use: [{
				loader: 'babel-loader',
				options: {
					presets: ['es2015']
				}
			}],
			exclude: /node_modules/
		}]
	},
	plugins: [
		new CleanWebpackPlugin()
	]
}
